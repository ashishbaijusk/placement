const mongoose=require('mongoose');
const Schema=mongoose.Schema;
mongoose.Promise=global.Promise;
const placedschema =   new Schema({
    student_USN:String,
    student_name:String,
    company_name:String,
    company_level:String,
    company_package:Number,
    placed_year:Number
});
const   Placed =   module.exports  =   mongoose.model("Placed",placedschema);


module.exports.addPlaced   =   (newplaced,callback)=>{
    Placed.create(newplaced, callback);
}
