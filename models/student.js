const mongoose=require('mongoose');
const Schema=mongoose.Schema;
mongoose.Promise=global.Promise;

const studentschema =   new Schema({
    student_USN:String,
    student_name:String,
    student_dob:Date,
    student_gender:String,
    student_email:String,
    student_place:String,
    student_state:String,
    student_phno:Number,
    student_10_yearofpassing:Number,
    student_10_cgp:Number,
    student_10_school:String,
    student_10_place:String,
    student_10_state:String,
    student_10_board:String,
    student_12_yearofpassing:Number,
    student_12_cgp:Number,
    student_12_school:String,
    student_12_place:String,
    student_12_state:String,
    student_12_board:String,
    student_BE_cgp:Number,
    student_BE_yearofpassing:Number,
    student_backlogs:Number,
    company_level:String
});

const   Student =   module.exports  =   mongoose.model("Student",studentschema);

module.exports.addStudent   =   (newstudent,callback)=>{
    Student.create(newstudent, callback);
}


module.exports.updateStudent    =   (USN,updatestudent,callback)=>{
    Student.findOne({student_USN:USN},(err,student)=>{
        if(err){
            throw   err;
        }
        else{
            id  =   student._id;
            Student.findByIdAndUpdate(id, updatestudent,callback);
        };
    });
};




module.exports.removeStudent    =   (student_USN,callback)=>{  
    Student.findOne({student_USN:student_USN}, (err,student)=>{
        if(err){
            throw err;
        }
        else{
            id  =   student._id;
            Student.findByIdAndRemove(id,callback);
        }
    });
}

// module.exports.showStudentdept  =   (student_department,student_CGP,student_backlogs,callback)=>{
//     Student.find({student_backlog:{lte:student_backlog},student_department:student_department,student_CGP:{gte:student_CGP}}).sort({student_CGP:1})
// }


// module.exports.showStudent=(student_backlogs,callback)=>{
//     Student.findOne({student_backlogs:student_backlogs},(err,student)=>{
//         if(err){
//             throw err;
//         }
//         else{

//         }
//     })
// }