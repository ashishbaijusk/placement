var express = require('express');
var router = express.Router();
var Student=require('../models/student');
var Placed=require('../models/placed');
var High=require('../models/high');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/addstudent',(req,res,next)=>{
  res.render('addstudent');
})

router.post('/addstudent',(req,res,next)=>{
  newstudent= new Student({
  student_USN:req.body.student_USN,
  student_name:req.body.student_name,
  student_dob:req.body.student_dob,
  student_gender:req.body.student_gender,
  student_email:req.body.student_email,
  student_place:req.body.student_place,
  student_state:req.body.student_state,
  student_phno:req.body.student_phno,
  student_10_yearofpassing:req.body.student_10_yearofpassing,
  student_10_cgp:req.body.student_10_cgp,
  student_10_school:req.body.student_10_school,
  student_10_place:req.body.student_10_place,
  student_10_state:req.body.student_10_state,
  student_10_board:req.body.student_10_board,
  student_12_yearofpassing:req.body.student_12_yearofpassing,
  student_12_cgp:req.body.student_12_cgp,
  student_12_school:req.body.student_12_school,
  student_12_place:req.body.student_12_place,
  student_12_state:req.body.student_12_state,
  student_12_board:req.body.student_12_board,
  student_BE_cgp: req.body.student_BE_cgp,
  student_backlogs:req.body.student_backlogs,
  student_BE_yearofpassing:req.body.student_BE_yearofpassing,
  company_level:'L4'
  })
  Student.find({student_USN:newstudent.student_USN},(err,stu)=>{
    if(err)
    {
      throw err;
    }
    if(stu=[])
    {
      Student.addStudent(newstudent,(err,student)=>{
        if(err){
          throw err;
        }
        else{
        
          res.redirect('/');
        }
      })
    }
    else
    {
      res.redirect('addstudent');
    }
  })
 
})

router.get('/showstudent',(req,res,next)=>{
  res.render('showstudent');
})

router.post('/showstudent',(req,res,next)=>{
  student_backlogs=req.body.student_backlogs;
  student_BE_cgp=req.body.student_BE_cgp;
  student_10_cgp=req.body.student_10_cgp;
  student_12_cgp=req.body.student_12_cgp;
  company_level=req.body.company_level;
  student_BE_yearofpassing=req.body.student_BE_yearofpassing;
  console.log(company_level);
  Student.find({student_backlogs:{$lte:student_backlogs},student_BE_cgp:{$gte:student_BE_cgp},student_10_cgp:{$gte:student_10_cgp},student_12_cgp:{$gte:student_12_cgp},company_level:{$gt:company_level},student_BE_yearofpassing:student_BE_yearofpassing},(err,student)=>{
    if(err){
      throw err;
    }
    else{
      sno=1;
      console.log(student);
      res.render("show",{student:student,sno:sno});
    }
  }).sort({student_USN:1});
})


router.get('/addcompany',(req,res,next)=>{
  res.render('addplaced');
})


router.post('/addcompany',(req,res,next)=>{
  newplaced = new Placed({
    student_USN:req.body.student_USN,
    student_name:req.body.student_name,
    company_name:req.body.company_name,
    company_level:req.body.company_level,
    company_package:req.body.company_package,
    placed_year:req.body.placed_year
  });

      Student.findOne({student_USN:newplaced.student_USN},(err,student)=>{
        if(err){
            throw   err;
        }
        else{
            id  =   student._id;
            Placed.addPlaced(newplaced,(err,company)=>{
                if(err)
                {
                  throw err;
                }
                else{
                  Student.findByIdAndUpdate(id,{company_level:newplaced.company_level},(err)=>{
                    if(err)
                    {
                      throw err;
                    }
                    else{
                    res.redirect('/');
                }
            });
        }
    });
  }
 });
});

  router.get('/showcompany',(req,res,next)=>{
   res.render('showcompany');
})


router.post('/showcompany',(req,res,next)=>{
  placed_year=req.body.placed_year;

  Placed.find({placed_year:placed_year},(err,company)=>{
    if(err){
      throw err;
    }
    else{
      sno=1;
      res.render("company",{company:company,sno:sno});
    }
  }).sort({company_name:1});
})

router.get('/showall',(req,res,next)=>{
  res.render('studentusn');
})

router.post('/showall',(req,res,next)=>{
  student_USN=req.body.student_USN;
  Student.find({student_USN:student_USN},(err,student)=>{
    if(err)
    {
      throw err;
    }
    else{
      res.render("showall",{student:student});
    }
  });
});

router.get('/delete',(req,res,next)=>{
  usn=req.param.student_USN;
  Student.removeStudent(usn,(err)=>{
    if(err)
            throw err
    else
            res.redirect('/showall');
//            res.json({success:true, message:'student Deleted Successfully'});

  });
});

router.get('/update',(req,res,next)=>{
          usn=req.param.student_USN;
          Student.find({student_USN:usn},(err,student)=>{
            if(err){
              throw err;
            }
            else
              res.render("updatestudent",{usn:usn});
          })
})


router.post('/update',(req,res,next)=>{
      student_USN=req.body.student_USN;
      newstudent= new Student({
        student_USN:req.body.student_USN,
        student_name:req.body.student_name,
        student_dob:req.body.student_dob,
        student_gender:req.body.student_gender,
        student_email:req.body.student_email,
        student_place:req.body.student_place,
        student_state:req.body.student_state,
        student_phno:req.body.student_phno,
        student_10_yearofpassing:req.body.student_10_yearofpassing,
        student_10_cgp:req.body.student_10_cgp,
        student_10_school:req.body.student_10_school,
        student_10_place:req.body.student_10_place,
        student_10_state:req.body.student_10_state,
        student_10_board:req.body.student_10_board,
        student_12_yearofpassing:req.body.student_12_yearofpassing,
        student_12_cgp:req.body.student_12_cgp,
        student_12_school:req.body.student_12_school,
        student_12_place:req.body.student_12_place,
        student_12_state:req.body.student_12_state,
        student_12_board:req.body.student_12_board,
        student_BE_cgp: req.body.student_BE_cgp,
        student_backlogs:req.body.student_backlogs,
        student_BE_yearofpassing:req.body.student_BE_yearofpassing,
        company_level:req.body.company_level
        })
        Student.updateStudent(student_USN,newstudent,(err)=>{
          if(err){
            throw err;
          }
          else
          res.redirect('/showall');
        })
})


router.post('/addhigher',(req,res,next)=>{
    newhigh  =  new High({
      student_USN:req.body.student_USN,
      student_name:req.body.student_name,
      university:req.body.university,
      year_of_passing:req.body.year_of_passing
    });
    Student.findOne({student_USN:newhigh.student_USN},(err,student)=>{
      if(err){
          throw   err;
      }
      else{
          id  =   student._id;
    High.addHigh(newhigh,(err)=>{
      if(err){
        throw err;
      }
      else{ 
        Student.findByIdAndUpdate(id,{company_level:'H'},(err)=>{
        if(err)
        {
          throw err;
        }
        else{
        res.redirect('/');
    }
      })
    }
  });
 }
 })
}); 

  router.get('/addhigher',(req,res,next)=>{
    res.render('high')
  })


router.get('/showhigher',(req,res,next)=>{
      res.render('showhigh');
});

router.post('/high',(req,res,next)=>{
  placed_year=req.body.placed_year;

  High.find({year_of_passing:placed_year},(err,high)=>{
    if(err){
      throw err;
    }
    else{
      sno=1;
      res.render("higher",{high:high,sno:sno});
    }
  })
});

router.get('/showall',(req,res,next)=>{
  res.render('studentusn');
})



module.exports = router;
